import React from 'react';

export default async function getAccred(id: number) {
    const res = await fetch(`https://accred-api-test.epfl.ch/nestjs-api/accreds/${id}`);

    console.log('API Response:', res);

    if (!res.ok) {
        throw new Error('Failed to fetch accred');
    }

    const data: Promise<Accred> = getAccred(id); // Use res.json() to directly parse the JSON response

    console.log('Accred by id:', data);

    return data; // Return the parsed data directly, no need to use JSON.parse
}
