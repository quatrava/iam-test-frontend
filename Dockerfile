# Use a Node.js runtime as the base image
FROM node:16.14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install application dependencies
RUN npm install

# Copy the entire application source code to the container
COPY . .

# Build the Next.js application
RUN npm run build

# Expose the port that your Next.js application will run on
EXPOSE 8080

# Command to run the Next.js application
CMD ["npm", "start"]

